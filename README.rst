App It - Coding Standard
========================

Version
-------

Version: 0.0.1

Prepared by Leo Sin

Summary
--------
This is a short document describing the preferred coding style for the codebases in the company for maintainability.

1. Indentation
--------------

Indentation is to clearly define the start and end of a block of control.

First of all, **no** mixed tab and spaces.

Programming Languages
~~~~~~~~~~~~~~~~~~~~~

- 8-space indentation should be used for programming languages

  - Rationale

    #. Easier to follow with wide indentation

       - The idea of indentation is to clearly define where the control block start and ends.

       - Especially when you've been looking at your screen for 8 hours straight, you'll find it a lot easier to see how the indentation works if you have large indentations.

    #. Avoids width issue on retab in editor that makes the diffs in Version Control messy

  - Some people claim 8-character indentations makes the code move too far to the right/hard to read on 80-character terminal screen

    - Answer: if one need more than 3 levels of indentation, it's probably screwed and should be fixed

- Exception: Function Parameter List Alignment

- The preferred way to ease multiple indentation level in a switch statement is to align the :code:`switch` and its subordinate :code:`case` labels in the same column instead of double-indenting the :code:`case` labels, e.g.

.. code-block :: C

   switch (expression) {
   case condition_1:
   case condition_2:
           statement_1;
           break;
   case condition_3:
   case condition_4:
           statement_2;
           // Fall through
   default:
           break;
   }

- Don't put multiple statements on a single line unless you have something to hide, e.g.

.. code-block :: C

   if (condition) do_this;
           do_something_everytime;

Markup Languages (HTML, MD, RST, etc.) and CSS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- 2 ASCII spaces

  - Rationale: 

    - 

2. Breaking long lines and Strings
----------------------------------

- Coding Style is all about readability and maintainability using commonly available tools.

- The limit on the length of lines: 80 columns, and this is strongly preferred limit.

- Statements longer than 80 columns will be broken into sensible chunks

  - Unless exceeding 80 columns significantly increases readability and does not hide information.

  - Descendants are always substantially shorter than the parent and are placed substantially to the right

    - Also applied to function headers with a long argument list.

    - However, never break static strings in logging statements, that breaks the ability to :code:`grep` them

3. Placing Braces and Spaces
----------------------------

- Unlike indent size, there are few technical reasons to choose one placement strategy over the other, but the preferred way, as shown to us by the prophets *Kernighan* and *Ritchie* (K&R), is to put the opening brace last on the line and put the closing brace first, thusly:

.. code-block :: C

   if (condition) {
           somne_statement;
   }

This applies to all blocks (except for C Language: this applies to all non-function statement blocks, if there are in the future).

- The closing brace is empty on a line of its own, except in the cases where it is followed by a continuation of the same statement, i.e.

  1. the :code:`while` in a :code:`do-while` statement, or

  2. an :code:`else` in an :code:`if` statement

.. code-block :: C

   do {
           body of do-loop
   } while (condition);


- Always surround braces around the if-block

  - Rationale: This prevents the brace addition/removal pollution from the version control diffs.

- Special Case for C Language: Functions

  - Rationale: \(a) K&R are right and (b) K&R are right

  - Functions in C are special anyway (you can't nest them in C)

.. code-block :: C

   int function(int x)
   {
           body of function
   }

3.1 Spaces
~~~~~~~~~~

Use of spaces depends (mostly) on function-versus-keyword usage.

- Keywords: Use a space after (most) keywords

  - Notable Exceptions (which look somewhat like functions and will be used with parentheses): :code:`sizeof, typeof, alignof, __attribute__`

- Thus, use a space after these keywords: :code:`if, switch, case, for, do, while, new, delete`

- Do not add spaces around (inside) **parenthesized** expressions.  This example is **bad**:

.. code-block :: C

   s = sizeof( struct file );

- Pointer Declarations (C-family): Asterisk adjacent to identifier

  When declaring pointer data or a function that returns a pointer type, the preferred use of :code:`*` is adjacent to the data name or function name, and not adjacent to the type name.  Examples:

.. code-block :: C

   char *banner;
   unsigned long long memparse(char *ptr, char **retptr);
   char *match_strdup(substring_t *s);

- Binary and Ternary Operators: Use one space around any of these:

.. code-block :: C

   = * - < > << >> * / % & | && || <= >= != ? :

- Unary Operators: No space after

.. code-block :: C

   & * + - ~. ! sizeof typeof alignof __attribute__ defined

- Increment & Decrement operators

  - Postfix: No space before
  - Prefix: No space after

- Member Operators (:code:`.` and :code:`->`): No space around

- Do **not** leave trailing whitespace at the ends of lines.

  - Git will warn you about patches that introduce trailing whitespace, and can optionally strip the trailing whitespace for you

- Javascript/Python - The inline object(json)/dictionary definition or the destructuring assignment or spreading: Leave surround a space inside the curly braces around the expressions 

4. Naming
---------

- Global/File Level Namespace: Descriptive names are a must

  - To call a global function :code:`foo` is a shooting offence
  - If you have a function that counts the number of active users, you should call that :code:`count_active_users()` or similar, but not :code:`cntusr()`
  - Note: Global variables is only to be used if you really need them

- Local Variables: Spartan Naming

  - Local variable names should be short, and to the point

    - If you have some random integer loop counter, it should probably be called :code:`i, j, k`
    - Calling it :code:`loop_counter` is non-productive, if there is no chance of it being mis-understood

  - No cute names like :code:`ThisVariableIsATemporaryCounter`

    - Just call it :code:`tmp`, which is much easier to write, and not the least more difficult to understand
    - :code:`tmp` can be just about any type of variable that is used to hold a temporary value

- Class Level Namespace (Object-Oriented)

  - member/property names: use noun or noun group
  - method names: whenever possible, use

    a. intransitive verb (vi.), or
    b. transitive verb (vt.) + object (noun)
       

- Casing:

  - Use lowercase names with words delimited by underscores
  - Mixed-case are discouraged for variable names and struct types, but preferred for class/typedef name


- Exception: Java
  - Java has its own naming convention (Camel Case) rooted in language level

- Do **NOT** encode the type of a function into the name (so-called Hungarian notation)

  - It is brain damaging
  - The compiler / interpreter knows the types anyway, and can check those
  - It only confuses the programmer

5. Typedefs (C-Family)
----------------------

- :code:`typedef` not necessarily help readability

  - Do **NOT** typedef pointers
  - Do not typedef structures most of the time (unless it is opaque)

    When you see, in the source, a 

    .. code-block :: C

       vps_t a;

    what does it mean?  In contrast if it says

    .. code-block :: C

       struct virtual_container *a;

    you can actually tell what :code:`a` is.

  In general, a pointer, or a struct that has elements that can reasonably be directly accessed should never be a typedef.

- :code:`typedef` are only useful for:

  a. totally opaque objects (typedef is to hide what the object is)
     
     Example: Opaque/Encapsulated objects

  b. Clear integer types, where the abstraction helps avoid confusion where it is :code:`int` or :code:`long`.

     However, there needs to be a reason for this.  If something is :code:`unsigned long`, then there's no reason to:

     .. code-block :: C

        typedef unsigned long myflags_t;

6. Function
------------

- Single Responsibility Principle (SRP)

  - Functions should be short and sweet

    - Most of the time they should fit on one or two screenfuls of text in the 80x24 terminal

  - And do **one** thing, and do that well

- The maximum length of a function is inversely proportional to the complexity and indentation of that function.

  - If you have a conceptually simple function that is just one long (but simple) case-statement, where you have to do a lot of small things for a lot of different cases, it's OK to have a longer function.

    - BUT in hashtable/dictionary enabled languages, probably there's a better way to rewrite it

- Another measure of the function is the number of local variables

  - They shouldn't exceed 5-10, or you're probably doing something wrong and might require re-thinking it and split it into smaller pieces
  - A human brain can generally easily keep track of about 7 different things, anything more and it gets confused.
  - We probably forget what's going on in such complex function 2 weeks later

7. Centralized exiting of functions (if no ExceptionHandlers)
---------------------------------------------------------------------

- Albeit deprecated by some people, the equivalent of the :code:`goto` statement is used frequently by compilers in form of the unconditional jump instruction.

- The :code:`goto` statment comes in handy when a function exits from multiple locations and some common work such as cleanup has to be done.

- If there is no cleanup needed, then just return directly.

- Choose label names which say what the goto does or why the goto exists.

  An example of good name could be :code:`out_free_buffer`

- Rationale

  - unconditional statements are easier to understand and follow
  - nested is reduced
  - update anomalies are prevented
  - save the compiler work to optimize redundant code away

.. code-block :: C

   int function(int a)
   {
           int rc = 0;
           char *buf;
   
           buffer = malloc(SIZE);
           if (!buf)
                   return -ENOMEM;
   
           if (condition) {
                   while (some_other_condition) {
                           ...
                   }
                   rc = 1;
                   goto out_free_buffer;
           }
           ...
   out_free_buffer:
           free(buf);
           return rc;
   }

- A common type of bug to be aware of is "one err bug", like

.. code-block :: C

   err:
           free(foo->bar);
           free(foo);
           return rc;

- The bug in this code is that on some exit paths :code:`foo` is :code:`NULL`. Normally the fix for this is to split it up into two error labels :code:`err_free_bar` and :code:`err_free_foo` :

.. code-block :: C

   err_free_bar:
          free(foo->bar);
   err_free_foo:
          free(foo);
          return rc;

- Ideally you should simulate errors to test all exit paths

8. Commenting
--------------

Comments are good, But there's also a danger of over-commenting

- Better to write self-explanatory code
- Comments are to tell *what* your code does and maybe *why* so, but not *how*

  Never try to explain *how* your code works in a comment, it's a waste of time to explain badly written code.

- Also *avoid* putting comments inside a function body, put them at the head of the function

  If the function is so complex that you have to separately comment parts of it, there's a very high chance that it should be re-designed by SRP.

- You can make small comments to note or warn about something particularly clever (or ugly), but try to avoid excess.

- Preferred style for comments (for C-style languages): Javadoc's standard

- It's also important to comment data, whether they are basic types or derived types

  - To this end, use just one data declaration per line (no commas for multiple data declarations)
  - This leaves you room for a small comment on each item, explaining its use.


References
----------

- Linux Kernel Coding Style

- The C Programming Language, Second Edition by Brian W. Kernighan and Dennis M. Ritchie. Prentice Hall, Inc., 1988. ISBN 0-13-110362-8 (paperback), 0-13-110370-9 (hardback).

- The Practice of Programming by Brian W. Kernighan and Rob Pike. Addison-Wesley, Inc., 1999. ISBN 0-201-61586-X.



